#ifndef _VALVE_ // _VALVE_CLASS
#define _VALVE_
#include"Subject.h"

class Valve : public Subject {
private:
	bool status;
public:
	Valve();
	Valve(bool);
	bool GetStatus();
	void SetStatus(bool status);
	~Valve();
};
#endif // _VALVE_CLASS

