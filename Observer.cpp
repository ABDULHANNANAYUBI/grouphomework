#include"Observer.h"
#include<iostream>

using namespace std;
int Observer::num = 0;
Observer::Observer(Subject &subject) : subject(subject) {
	this->subject.Attach(this);
	++this->num;
}

void Observer::Update(const string& massage , const int number) {
	this->massage = massage;
	this->number = number;
	//this->PrintInformation();
}

void Observer::Remove() {
	static int num = 0;
	if (num == 0) {
		this->subject.Detach(this);
		cout << this->massage  << " Simulation stopped" << endl;
		num++;
	}
	else {
		this->subject.Detach(this);
		cout << this->massage << " " << this->number << " Simulation stopped" << endl;
	}
}

