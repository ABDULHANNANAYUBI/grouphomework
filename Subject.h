#pragma once
#define _SUBJECT_
#include"SubjectInterface.h"
#include"ObserveInterface.h"
#include<iostream>
#include<list>
#include<vector>

class Subject :public SubjectInteface {
private:
	std::vector <ObserveInterface*> listOfObserved;
	std::string massage;
	int number;
public:

	virtual ~Subject() {};
	void Attach(ObserveInterface* observeInterface) override;
	void Detach(ObserveInterface* observeInterface) override;
	void Notify() override;
	void Massage(std::string);
};
