#pragma once
#define _OBSERVEINTERFACE_
#include<iostream>
class ObserveInterface {
public:
	virtual ~ObserveInterface() {};
	virtual void Update(const std::string& massage , const int) = 0;
};

