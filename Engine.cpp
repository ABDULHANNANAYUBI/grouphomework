﻿#include "Engine.h"
#include <time.h>
#include <iostream>
#include<fstream>
#include <iomanip>

using namespace std;
/// Default values which using in Program...
/// @param DEFAULT
/// @param DEFAULTCAPACITY
/// @param DEFAULTFUELPERSECOND
/// @param MAXIMUL
/// 
#define DEFAULT 1
#define MINFUEL 20
#define DEFAULTCAPACITY 55.0
#define DEFAULTFUELPERSECOND 5.5
#define MAXIMUM 20
/// <Instance Logic>
/// In here We use the instance for the purpose of the using the engine for the once
/// </Closing instance>
Engine* Engine::instance = 0;
/**
* The functions which we Get the instance the implementation of the function....
* The instance of this function create only once in engine
*/
Engine* Engine::getInstance() {
	if (instance == 0) {
		instance = new Engine();
	}
	return instance; /// Returnin the instance value of engine...
}

/// <Default constructor...>
/// The Default constructor of the Engine Class which is the first value of the every attribute inside the 
/// engine class
/// </Closing the Default Constructor>
Engine::Engine() {
	fuel_per_second = DEFAULTFUELPERSECOND;
	TotalConsumed = numOfConnectedTank = ConnectedTankID = numOfTank = 0;
	status = false;
	TotalFuelQuantity = DEFAULTCAPACITY;
	tank = new Tank[DEFAULT];
	tank[0].SetCapacity(DEFAULTCAPACITY);/// setting the default capacity
	tank[0].SetTankId(0);
	tank[0].SetFuelQuantity(DEFAULTCAPACITY);/// Setting the first tank id as zero(0)
	output.open("output.txt", ios::out, ios::trunc);
}
/**
* Paramatrized Constructor of the engine class
* In here we initialize the given constructor paramaters
* Closing the Paramitraized Constructor
*/
Engine::Engine(double fuel_per_second, bool status, int numOfTank, int numOfConnectedTank, double TotalConsumed) {

	this->fuel_per_second = fuel_per_second;
	this->status = status;
	this->numOfTank = numOfTank;
	this->numOfConnectedTank = numOfConnectedTank;
	this->TotalConsumed = TotalConsumed;
	this->ConnectedTankID = 0; /// using teh connnect tank id as zero for the first time
	this->TotalFuelQuantity = DEFAULTCAPACITY;
	tank = new Tank[DEFAULT]; /// creatin the dynamic...
	tank[0].SetCapacity(DEFAULTCAPACITY); /// setting the default capacity
	tank[0].SetTankId(this->numOfTank); /// Setting the first tank id as zero(0)
	output.open("output.txt", ios::out, ios::trunc);

}
/// <Seter Methods of the functions>
/// Setter Mether
/// </summary>
/// <@param name="fuel_per_second"></param>
void Engine::SetFuelPerSecond(double fuel_per_second) {
	this->fuel_per_second = fuel_per_second;
}

double Engine::GetFuelPerSecond() { /// Getter Methods of the function
	return this->fuel_per_second;
}

void Engine::SetStatus(bool status) {
	this->status = status;
}

bool Engine::GetStatus() {/// Getter Methods of the function
	return this->status;
}

void Engine::SetNumOfTank(int numOfTank) { ///Setter Methos of the function
	this->numOfTank = numOfTank;
}

int Engine::GetNumOfTank() {/// Getter Methods of the function
	return this->numOfTank;
}

void Engine::SetNumOfConnectedTank(int numOfConnectedTank) {///Setter Methos of the function
	this->numOfConnectedTank = numOfConnectedTank;
}

int Engine::GetNumOfConnectedTank() {/// Getter Methods of the function
	return this->numOfConnectedTank;
}

double Engine::GetTotalConsumed() {/// Getter Methods of the function
	return this->TotalConsumed;
}

void Engine::SetConnectedTankID(int id) {///Setter Methos of the function
	this->ConnectedTankID = id;
}

int Engine::GetConnectedTankID() {/// Getter Methods of the function
	return this->ConnectedTankID;
}

void Engine::SetTotalFuelQuantity(double TotalFuelQuantity) {
	this->TotalFuelQuantity = TotalFuelQuantity;///Setter Methos of the function
}

double Engine::GetTotalFuelQuantity() {/// Getter Methods of the function
	return this->TotalFuelQuantity;
}

/// <Add Fuel Tank>
/// Add Fuel Tank adding new tank to the engine with using the dynamic memory allcation
/// </Closing the Add Fuel Tank>
/// <@param name="capacity"></@param>

void Engine::AddFuelTank(double capacity) {
	this->numOfTank++;
	Tank* temp = new Tank[this->numOfTank + 1];
	for (int i = 0; i < this->numOfTank; i++) {
		temp[i] = tank[i];
	}
	delete[]tank; /// Deleting the Dynamic memory for prevent the memory leak...
	tank = new Tank[numOfTank + 1];
	for (int i = 0; i < numOfTank; i++) {
		tank[i] = temp[i];
	}
	tank[numOfTank].SetCapacity(capacity);
	tank[numOfTank].SetTankId(numOfTank);
	delete[] temp;/// Deleting the Dynamic memory for prevent the memory leak...

}
/// <Absorb Fuel>
/// Absorb Fuel function is decrement the total fuel quantity in each function call or step in the second
/// </Closing the Absorb Fuel>
/// <Boolean Value></Boolean >
bool Engine::AbsorbFuel() {
	int static random = 0;
	int i = 0;
	bool True = false;
	if (ConnectedTankID == 0 && tank[ConnectedTankID].GetFuelQuantity() > MINFUEL) {
		tank[ConnectedTankID].SetFuelQuantity(tank[ConnectedTankID].GetFuelQuantity() - DEFAULTFUELPERSECOND);
		TotalConsumed += DEFAULTFUELPERSECOND;
		TotalFuelQuantity -= DEFAULTFUELPERSECOND;
	}
	else {
		if (!random) { /// If not equal zero
			srand(time(NULL));
			random = rand() % numOfTank + 1; /// Random Function of the Engine..
			ConnectedTankID = random;
		}
		while (true) {
			if (tank[ConnectedTankID].GetFuelQuantity() >= 5.5 && tank[ConnectedTankID].GetConnectStatus() == true) {
				if (tank[ConnectedTankID].GetValveStatus() == true && tank[ConnectedTankID].GetBroken() == false)
					break;
			}
			else {
				srand(time(NULL));
				random = rand() % numOfTank + 1;
				ConnectedTankID = random;
			}
			i++;
			if (i > MAXIMUM) { /// Maximum of the in preprocessor
				for (int i = 1; i < numOfTank; i++) {
					if (tank[i].GetFuelQuantity() >= DEFAULTFUELPERSECOND && tank[i].GetConnectStatus() == true) {
						if (tank[i].GetValveStatus() == true && tank[i].GetBroken() == false) {
							ConnectedTankID = i;
							True = true;
							break;
						}
					}
				}
				break;
			}
		}
		if (!True && tank[ConnectedTankID].GetFuelQuantity() >= DEFAULTFUELPERSECOND) {
			tank[ConnectedTankID].SetFuelQuantity(tank[ConnectedTankID].GetFuelQuantity() - DEFAULTFUELPERSECOND);
			TotalConsumed += DEFAULTFUELPERSECOND;
			TotalFuelQuantity -= DEFAULTFUELPERSECOND;
		}
		else {
			return false;
		}
	}
}

/// <Paramitarized Absorb Fuel>
/// Absorb Fuel function is decrement the total fuel quantity in each function call or step in the given Second
/// </Closing the Absorb Fuel>
/// <Boolean Value></Boolean >
void Engine::AbsorbFuel(int seconds) {
	bool deger = false;
	for (int i = 0; i < seconds; i++) {
		deger = AbsorbFuel();
	}
	if (!deger) {
		cout << "Not Enough Tank To Reduce \n";
		output << "Not Enough Tank To Reduce \n";
	}
}
/// <Fill Tank>
/// Fill tank fill the tank with given id and the quantity as a paramiterized
/// </Closing the Fill Tank>
/// <@param name="TankId"></@param>
/// <@param name="quantity"></@param>
void Engine::FillTank(int TankId, double quantity) {
	if (TankId <= numOfTank && TankId >= 0) {
		if (quantity <= tank[TankId].GetCapacity() - tank[TankId].GetFuelQuantity()) {
			tank[TankId].SetFuelQuantity(tank[TankId].GetFuelQuantity() + quantity);
			TotalFuelQuantity = TotalFuelQuantity + quantity;
			cout << "The tank filled successfully!\n";
			output << "The tank filled successfully!\n";

		}
		else {
			cout << "Not Enough Capacity " << endl;/// Giving error
			output << "Not Enough Capacity " << endl;/// Giving error


			return;
		}
	}
	else {
		cout << "Wrong ID" << endl; /// Giving error
		output << "Wrong ID" << endl; /// Giving error
		return;
	}
}
/// <Connect Fuel Tank To Engine>
/// This function is connecting the tanks as a given tank id
/// </Closing connect Fuel Tank>
/// <@param name="Id"></@param>
void Engine::ConnectFuelTankToEngine(int Id) {
	if (tank[Id].GetTankId() != NULL && numOfTank >= Id) {
		tank[Id].SetConnectStatus(true);
		cout << "The tank connected to engine successfully!\n";
		output << "The tank connected to engine successfully!\n";

		numOfConnectedTank++; /// Incrementing the @param name = numofconnectedTank
	}
	else {
		cout << "Invalid ID" << endl;/// Giving error
		output << "Invalid ID" << endl;/// Giving error

	}
}
/// <Disconnect Fuel Tank To Engine>
/// This function is Disconnecting the tanks as a given tank id
/// </Closing Disconnect Fuel Tank>
/// <@param name="Id"></@param>
void Engine::DisconnectFuelTankFromEngine(int Id) {
	if (tank[Id].GetTankId() != NULL && numOfTank >= Id) {
		tank[Id].SetConnectStatus(false);
		cout << "The tank disconnected from engine successfully!\n";
		output << "The tank disconnected from engine successfully!\n";

		numOfConnectedTank--;/// Decrementing the @param name = numofconnectedTank
	}
	else {
		cout << "Invalid ID" << endl;/// Giving error
		output << "Invalid ID" << endl;/// Giving error
	}
}

/**
* Removing the Fuel Tank
* This function is Removing the Fuel Tank as a given Id
* void type function
*
*/
void Engine::RemoveFuelTank(int Id) {
	if (tank[Id].GetTankId() != NULL && numOfTank >= Id) {
		tank[Id].SetCapacity(NULL), tank[Id].SetConnectStatus(false), tank[Id].SetFuelQuantity(NULL), tank[Id].SetTankId(NULL);
		tank[Id].SetValveStatus(false), tank[Id].SetBroken(false);
	}
	else {
		cout << "Invalid ID" << endl;/// Giving error
		output << "Invalid ID" << endl;/// Giving error
	}
}
/// <Give Back Fuel>
/// This function is giving the give bakc fuel for the connected tank id 
/// 
/// </Closing the Give back Fuel>
/// <@param name="Quantity"></@param>
void Engine::GiveBackFuel(double Quantity) {
	int min = INT32_MAX; /// Using the int max for the finding the minumum of tank fuel ....
	int id = 0, i = 0;
	if (tank[0].GetFuelQuantity() >= Quantity) {
		for (i = 0; i < numOfTank; i++) {
			if (tank[i].GetFuelQuantity() < min) {
				min = tank[i].GetFuelQuantity();
				id = i;
			}
		}
		tank[0].SetFuelQuantity(tank[0].GetFuelQuantity() - Quantity);
		FillTank(id, Quantity); /// Using the previous Function 
	}
	else {
		cout << "Invalid Quantity ...!" << endl;/// Giving error
		output << "Invalid Quantity ...!" << endl;/// Giving error
	}
}
/// <Open Valve>
/// This Function is using for opening the valve by given tank id
/// 
/// </Closing the function >
/// <@param name="Id"></@param>
void Engine::OpenValve(int Id) {
	if (tank[Id].GetTankId() != NULL && numOfTank >= Id) {
		tank[Id].SetValveStatus(true);
		cout << "The valve opened successfully!\n";
		output << "The valve opened successfully!\n";
	}
	else {
		cout << "Invalid Id";/// Giving error
		output << "Invalid Id";/// Giving error
	}

}
/// <Close Valve >
/// This Function is using for opening the valve by given tank id
/// </Closing Valve>
/// <@param name="Id"></@param>
void Engine::CloseValve(int Id) {
	if (tank[Id].GetTankId() != NULL && numOfTank >= Id) {
		tank[Id].SetValveStatus(false);
		cout << "The valve closed successfully!\n";
		output << "The valve closed successfully!\n";
	}
	else {
		cout << "Invalid Id";/// Giving error
		output << "Invalid Id";/// Giving error
	}
}

/// <List Fuel Tanks>
/// This function is printing the fuel tanks ...
/// the return type is void
/// 
/// </Closing List Fuel Tanks>
void Engine::ListFuelTanks() {
	cout << "The Tank ID(s) and information(s) are as follows:\n\n"
		<< "ID(s)" << setw(20) << "Capacity(s)" << setw(35) << "Current Amount(s) of Fuel" << setw(20) << "Is Connected?" << setw(20) << "Is Broken?\n"
		<< "-----" << setw(20) << "-----------" << setw(35) << "-------------------------" << setw(20) << "-------------" << setw(20) << "----------\n";
	output << "The Tank ID(s) and information(s) are as follows:\n\n"
		<< "ID(s)" << setw(20) << "Capacity(s)" << setw(35) << "Current Amount(s) of Fuel" << setw(20) << "Is Connected?" << setw(20) << "Is Broken?\n"
		<< "-----" << setw(20) << "-----------" << setw(35) << "-------------------------" << setw(20) << "-------------" << setw(20) << "----------\n";
	for (int i = 0; i < numOfTank; i++) {
		cout << setw(3) << tank[i].GetTankId() << setw(18) << tank[i].GetCapacity() << setw(28) << tank[i].GetFuelQuantity() << setw(25) << tank[i].GetConnectStatus() << setw(22) << tank[i].GetBroken() << endl;
		output << setw(3) << tank[i].GetTankId() << setw(18) << tank[i].GetCapacity() << setw(28) << tank[i].GetFuelQuantity() << setw(25) << tank[i].GetConnectStatus() << setw(22) << tank[i].GetBroken() << endl;
	}
}
/// <Print Fuel Tank Count>
/// This function is printing the fuel tanks count ...
/// </Close Print Fuel Tank Count>
void Engine::PrintFuelTankCount() {
	int counter = 0;
	for (int i = 0; i < numOfTank; i++) {
		if (tank[i].GetTankId() != NULL) {
			counter++; /// incrementin the counter...
		}
	}
	cout << endl << "The Total Fuel Tank : " << counter << endl;
	output << endl << "The Total Fuel Tank : " << counter << endl;
}

/// <List Connected Tanks>
/// This function is printing the fuel tanks count ...
/// </Close List Connected Tanks>
void Engine::ListConnectedTanks() {
	cout << "The Connected Tank ID(s) and information(s) are as follows:\n\n"
		<< "ID(s)" << setw(20) << "Capacity(s)" << setw(35) << "Current Amount(s) of Fuel" << setw(20) << "Is Connected?" << setw(20) << "Is Broken?\n"
		<< "-----" << setw(20) << "-----------" << setw(35) << "-------------------------" << setw(20) << "-------------" << setw(20) << "----------\n";
	output << "The Connected Tank ID(s) and information(s) are as follows:\n\n"
		<< "ID(s)" << setw(20) << "Capacity(s)" << setw(35) << "Current Amount(s) of Fuel" << setw(20) << "Is Connected?" << setw(20) << "Is Broken?\n"
		<< "-----" << setw(20) << "-----------" << setw(35) << "-------------------------" << setw(20) << "-------------" << setw(20) << "----------\n";
	for (int i = 1; i < numOfTank; i++) {
		if (tank[i].GetConnectStatus() && tank[i].GetTankId() != NULL) {
			cout << setw(3) << tank[i].GetTankId() << setw(18) << tank[i].GetCapacity() << setw(28) << tank[i].GetFuelQuantity() << setw(25) << tank[i].GetConnectStatus() << setw(22) << tank[i].GetBroken() << endl;
			output << setw(3) << tank[i].GetTankId() << setw(18) << tank[i].GetCapacity() << setw(28) << tank[i].GetFuelQuantity() << setw(25) << tank[i].GetConnectStatus() << setw(22) << tank[i].GetBroken() << endl;
		}
	}
}
/// <Print Total Fuel Quantity>
/// This function is printing the fuel tanks count ...
/// </Close Print Total Fuel Quantity>
void Engine::PrintTotalFuelQuantity() {
	cout << "Total Fuel Quantity : " << TotalFuelQuantity << endl;
	output << "Total Fuel Quantity : " << TotalFuelQuantity << endl;
}
/// <Print Total Consumed Fuel Quantity>
/// This function is printing the consumed fuel tanks count ...
/// </Close Print Total Consumed Fuel Quantity>
void Engine::PrintTotalConsumedFuelQuantity() {
	cout << "Total Consumed Fuel Quantity : " << TotalConsumed << endl;
	output << "Total Consumed Fuel Quantity : " << TotalConsumed << endl;
}
/// <Print Tank Info>
/// This function is printing the fuel tanks count ...
/// </summary>
/// <@param name="Id"></@param>
void Engine::PrintTankInfo(int Id) {
	cout << "The Tank ID and information(s) are as follows:\n\n"
		<< "ID" << setw(20) << "Capacity" << setw(35) << "Current Amount of Fuel" << setw(20) << "Is Connected?" << setw(20) << "Is Broken?\n"
		<< "-----" << setw(20) << "-----------" << setw(35) << "-------------------------" << setw(20) << "-------------" << setw(20) << "----------\n";
	output << "The Tank ID and information(s) are as follows:\n\n"
		<< "ID" << setw(20) << "Capacity" << setw(35) << "Current Amount of Fuel" << setw(20) << "Is Connected?" << setw(20) << "Is Broken?\n"
		<< "-----" << setw(20) << "-----------" << setw(35) << "-------------------------" << setw(20) << "-------------" << setw(20) << "----------\n";
	if (tank[Id].GetTankId() != NULL) {
		cout << setw(3) << tank[Id].GetTankId() << setw(18) << tank[Id].GetCapacity() << setw(28) << tank[Id].GetFuelQuantity() << setw(25) << tank[Id].GetConnectStatus() << setw(22) << tank[Id].GetBroken() << endl;
		output << setw(3) << tank[Id].GetTankId() << setw(18) << tank[Id].GetCapacity() << setw(28) << tank[Id].GetFuelQuantity() << setw(25) << tank[Id].GetConnectStatus() << setw(22) << tank[Id].GetBroken() << endl;
	}

}
/// <Break Fuel Tank>
/// this function is using for break the fuel tank
/// </Break Fuel Tank>
/// <@param name="Id"></@param>
void Engine::BreakFuelTank(int Id) {
	if (tank[Id].GetTankId() != NULL && numOfTank <= Id) {
		tank[Id].SetBroken(true);
		cout << "The tank has broken successfully!\n";
		output << "The tank has broken successfully!\n";
	}
	else {
		cout << "Invalid Id" << endl; /// incrementin the counter...
		output << "Invalid Id" << endl; /// incrementin the counter...
	}
}

void Engine::RepairFuelTank(int Id) {
	if (tank[Id].GetTankId() != NULL && numOfTank <= Id) {
		tank[Id].SetBroken(false);
		cout << "The tank has repaired successfully!\n";
		output << "The tank has repaired successfully!\n";
	}
	else {
		cout << "Invalid Id" << endl;/// incrementin the counter...
		output << "Invalid Id" << endl;/// incrementin the counter...
	}
}

fstream& Engine::GetOutput() {
	return this->output;
}

void Engine::SetOutput() {
	this->output.close();
}

Engine :: ~Engine() {
	delete[]tank; /// Deleting the dynamic memory...
	SetOutput();
}