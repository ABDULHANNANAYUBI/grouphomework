#pragma once
#include "Tank.h"
#include<string>
#include<fstream>
#include"Subject.h"

using namespace std;
class Engine :public Subject {
private:
	/// <Private Attributes>
	double fuel_per_second, TotalConsumed, TotalFuelQuantity;
	bool status;
	int numOfTank;
	int numOfConnectedTank;
	int ConnectedTankID;
	Tank* tank;
	fstream output;
	static Engine* instance; /// Using the Instance for the Engine as private...
	Engine();/// Using the Instance for the Engine as private...
	Engine(double, bool, int, int, double);/// Using the Instance for the Engine as private...

public:
	static Engine* getInstance(); /// Using the Instance for the Engine as Public...
	~Engine();
	void SetFuelPerSecond(double); /// Getter The Instance for the Engine
	double GetFuelPerSecond();
	void SetStatus(bool);
	bool GetStatus();/// Getter The Instance for the Engine
	void SetNumOfTank(int);
	int GetNumOfTank();/// Getter The Instance for the Engine
	void SetNumOfConnectedTank(int);
	int GetNumOfConnectedTank();
	double GetTotalConsumed();
	void SetConnectedTankID(int);
	int GetConnectedTankID();/// Getter The Instance for the Engine
	void SetTotalFuelQuantity(double);
	double GetTotalFuelQuantity();
	void AddFuelTank(double);
	bool AbsorbFuel();
	void AbsorbFuel(int);
	void FillTank(int, double);
	void ConnectFuelTankToEngine(int);
	void DisconnectFuelTankFromEngine(int);
	void RemoveFuelTank(int);
	void GiveBackFuel(double);
	void OpenValve(int);
	void CloseValve(int);
	void ListFuelTanks();
	void PrintFuelTankCount();
	void ListConnectedTanks();
	void PrintTotalFuelQuantity();
	void PrintTotalConsumedFuelQuantity();
	void PrintTankInfo(int);
	void BreakFuelTank(int);
	void RepairFuelTank(int);
	fstream& GetOutput();
	void SetOutput();
	void getTank(Tank &tank , int id) { 
		tank = this->tank[id];
	}
	void getValve(Valve &valve, int id) { 
		valve = this->tank[id].getvalve();
	}
};
