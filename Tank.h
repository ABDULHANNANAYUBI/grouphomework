#ifndef _TANK_ // _TANK_CLASS
#define _TANK_
#include"Subject.h"
#include "Valve.h"

class Tank:public Subject {
private:
	double capacity;
	double fuel_quantity;
	bool broken;
	int TankId;
	bool ConnectStatus;
	Valve* valve;
public:	
	Tank();
	Tank(double, double, bool, int);
	void SetCapacity(double);
	void SetFuelQuantity(double);
	void SetBroken(bool);
	void SetTankId(int);
	double GetCapacity();
	double GetFuelQuantity();
	bool GetBroken();
	int GetTankId();
	void SetConnectStatus(bool);
	bool GetConnectStatus();
	void SetValveStatus(bool);
	bool GetValveStatus();
	Valve *getvalve() { return this->valve; }
	~Tank();
};
#endif // _TANK_CLASS

