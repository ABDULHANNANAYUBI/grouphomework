#ifndef INPUT_H
#define INPUT_H

#include <string>
#include <fstream>
#include"Engine.h"
#include"Subject.h"
#include"Observer.h"
#include<vector>
using namespace std;

class Input:public Subject {
private:
	Engine* engine = Engine::getInstance();
	vector <Observer*> observe;
	string inputStr;
	fstream fileToBeRead;
	double leftNumberInCommand, rightNumberInCommand; /// leftNumberInCommand and rightNumberInCommand takes the numbers in commands and save to use later.
public:
	Input(); /// Default Constructor
	Input(string); /// Parametirized Constructor (Gets the name of command list file as user's request).
	~Input();
	void SetStr(string);
	string GetStr(string);
	void ReadFunction(); /// Reads the command from the input file.
	bool whichCommand(string); /// Runs (brings) the command from the Engine Class.
	void HowManyQuantity(string); /// Reads left and right number in commands and save it inside private members. (leftNumberInCommand, rightNumberInCommand)
};

#endif