#include"Tank.h"
#include<iostream>

using namespace std;

/// Implementation of Default and Parametrized Constructors
Tank::Tank() {
	capacity = fuel_quantity = TankId = 0.0;
	broken = false;
	ConnectStatus = false; /// each tank starts with no engine attached.
	this->valve = new Valve();
}
Tank::Tank(double capacity, double fuel_quantity, bool broken, int Tank_Id) {
	this->capacity = capacity;
	this->fuel_quantity = fuel_quantity;
	this->TankId = Tank_Id;
	this->broken = broken;
	ConnectStatus = false;
	this->valve = new Valve();
}

/// Implementation of Setter Methods
void Tank::SetCapacity(double capacity) {
	this->capacity = capacity;
}
void Tank::SetFuelQuantity(double fuel_quantity) {
	this->fuel_quantity = fuel_quantity;
}
void Tank::SetBroken(bool broken) {
	this->broken = broken;
}
void Tank::SetTankId(int Tank_Id) {
	this->TankId = Tank_Id;
}
void Tank::SetConnectStatus(bool ConnectionStatus) {
	this->ConnectStatus = ConnectionStatus;
}
void Tank::SetValveStatus(bool ValveStatus) {
	this->valve->SetStatus(ValveStatus);
}

/// Implementation of Getter Methods
double Tank::GetCapacity() {
	return capacity;
}
double Tank::GetFuelQuantity() {
	return fuel_quantity;
}
int Tank::GetTankId() {
	return TankId;
}
bool Tank::GetBroken() {
	return broken;
}
bool Tank::GetConnectStatus() {
	return ConnectStatus;
}
bool Tank::GetValveStatus() {
	return valve->GetStatus();
}


Tank::~Tank() { 
	Subject::Massage("Tank");
	Notify(); 
} /// destructor