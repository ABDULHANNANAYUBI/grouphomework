#include "Valve.h"

Valve::Valve() {
	
	status = false; /// each valve closes(false) when first created.
}
Valve::Valve(bool status) {
	this->status = status;
}

void Valve::SetStatus(bool status) { /// setter method
	this->status = status;
	
}

bool Valve::GetStatus() { /// getter method
	return this->status;
}

Valve::~Valve() {
	Subject::Massage("Valve");
	Notify();
}/// destructor
