#pragma once
#define _OBSERVER_
#include"ObserveInterface.h"
#include"Subject.h"

class Observer :public ObserveInterface {
private:
	Subject& subject;
	std::string massage;
	static int num;
	int number;
public:
	Observer(Subject&);
	virtual ~Observer() {}
	void Update(const std::string& , const int) override;
	void Remove();
};


