#include <iostream>
#include <stdlib.h>
#include <string>
#include <fstream>
#include <iomanip>
#include "Input.h"
#include "Engine.h"
#include "Valve.h"
using namespace std;

Input::Input():leftNumberInCommand(0), rightNumberInCommand(0) {
	fileToBeRead.open("input.txt", ios::in);
	Observer* observe1 = new Observer(*engine);
	observe1->Update("Engine", 0);
	this->observe.push_back(observe1);
	if (!fileToBeRead) {
		cout << "The file to be read was not found or the file is defective! (input.txt)\n";
		engine->GetOutput() << "The file to be read was not found or the file is defective! (input.txt)\n";

		exit(0);
	}
	else {
		cout << "The file was opened successfully! (input.txt)\n";
		engine->GetOutput() << "The file was opened successfully! (input.txt)\n";
	}
}

Input::Input(const string tempFileName) :leftNumberInCommand(0), rightNumberInCommand(0) {
	fileToBeRead.open(tempFileName, ios::in);
	//Attach(Input::engine());
	Observer* observe1 = new Observer(*engine);
	observe1->Update("Engine" , 0);
	this->observe.push_back(observe1);
	if (!fileToBeRead) {
		
		cout << "The file to be read was not found or the file is defective! (" << tempFileName << ")\n";
		engine->GetOutput() << "The file to be read was not found or the file is defective! (" << tempFileName << ")\n";
		exit(0);
	}
	else {
		cout << "The file was opened successfully! (" << tempFileName << ")\n";
		engine->GetOutput() << "The file was opened successfully! (" << tempFileName << ")\n";
	}

}

void Input::SetStr(string tempStr) {
	this->inputStr = tempStr;
}

string Input::GetStr(string tempStr) {
	return inputStr;
}

void Input::ReadFunction() {
	string tempStr;

	getline(fileToBeRead, tempStr);
	while (whichCommand(tempStr)) {
		getline(fileToBeRead, tempStr);
	}

	if (whichCommand(tempStr)) { /// If the input will be "stop_simulation;" whichCommand function returns false and the program ends.
		cout << "Simulation is stopped!";
		engine->GetOutput() << "Simulation is stopped!";

		exit(0);
	}
}

bool Input::whichCommand(const string tempStr) {
	HowManyQuantity(tempStr);
	if (tempStr == "start_engine;") {
		if (engine->GetNumOfConnectedTank() > 0 && !engine->GetStatus()) {
			engine->SetStatus(true);
			cout << "Engine started!\n";
			engine->GetOutput() << "Engine started!\n";
			engine->AbsorbFuel();
		}
		else if (engine->GetStatus()) {
			cout << "Engine is already running!\n";
			engine->GetOutput() << "Engine is already running!\n";
		}
		else {
			cout << "The tank required for engine operation was not found!\n";
			engine->GetOutput() << "The tank required for engine operation was not found!\n";
		}
	}
	if (tempStr == "stop_engine;") {
		if (engine->GetNumOfConnectedTank() <= 0 || !engine->GetStatus()) {
			cout << "The engine is not already running!\n";
			engine->GetOutput() << "The engine is not already running!\n";
		}
		else {
			engine->AbsorbFuel();
			engine->SetStatus(false);
			cout << "Engine stopped!\n";
			engine->GetOutput() << "Engine stopped!\n";
		}
	}
	if (!tempStr.find("give_back_fuel")) {
		if (engine->GetNumOfConnectedTank() > 0 && engine->GetStatus()) {
			engine->AbsorbFuel();
			engine->GiveBackFuel(leftNumberInCommand);
			cout << "The fuel gave back!\n";
			engine->GetOutput() << "The fuel gave back!\n";
		}
		else {
			cout << "The engine is not already running!\n";
			engine->GetOutput() << "The engine is not already running!\n";
		}
	}
	if (!tempStr.find("add_fuel_tank")) {
		engine->AddFuelTank(leftNumberInCommand);
		if (engine->GetNumOfConnectedTank() > 0 && engine->GetStatus()) {
			engine->AbsorbFuel();
		}
		
		cout << "Fuel added!\n";
		engine->GetOutput() << "Fuel added!\n";
	}
	if (tempStr == "list_fuel_tanks;") {
		if (engine->GetNumOfConnectedTank() > 0 && engine->GetStatus())
			engine->AbsorbFuel();

		engine->ListFuelTanks();
	}
	if (tempStr == "print_fuel_tank_count;") {
		if (engine->GetNumOfConnectedTank() > 0 && engine->GetStatus())
			engine->AbsorbFuel();

		cout << "Total Number of Tank(s): " << engine->GetNumOfTank() << endl;
		cout << "Total Number of ConnectedTank(s): " << engine->GetNumOfConnectedTank() << endl;
		engine->GetOutput() << "Total Number of Tank(s): " << engine->GetNumOfTank() << endl;
		engine->GetOutput() << "Total Number of ConnectedTank(s): " << engine->GetNumOfConnectedTank() << endl;

	}
	if (!tempStr.find("remove_fuel_tank")) {
		if (engine->GetNumOfConnectedTank() > 0) {
			engine->AbsorbFuel();
			engine->RemoveFuelTank(leftNumberInCommand);
			cout << "The tank removed successfully!\n";
			engine->GetOutput() << "The tank removed successfully!\n";
		}
		else {
			cout << "The engine has not enough connect tanks to remove!\n";
			engine->GetOutput() << "The engine has not enough connect tanks to remove!\n";
		}
	}
	if (!tempStr.find("connect_fuel_tank_to_engine")) {
		engine->ConnectFuelTankToEngine(leftNumberInCommand);

		if (engine->GetNumOfConnectedTank() > 0 && engine->GetStatus()) {
			engine->AbsorbFuel();
		}
	}
	if (!tempStr.find("disconnect_fuel_tank_from_engine")) {
		engine->DisconnectFuelTankFromEngine(leftNumberInCommand);
		if (engine->GetNumOfConnectedTank() > 0 && engine->GetStatus()) {
			engine->AbsorbFuel();
		}
	}
	if (tempStr == "list_connected_tanks;") {
		if (engine->GetNumOfConnectedTank() > 0 && engine->GetStatus())
			engine->AbsorbFuel();

		engine->ListConnectedTanks();
	}
	if (tempStr == "print_total_fuel_quantity;") {
		if (engine->GetNumOfConnectedTank() > 0 && engine->GetStatus())
			engine->AbsorbFuel();

		cout << "Total Fuel Quantity: " << engine->GetTotalFuelQuantity() << endl;
		engine->GetOutput() << "Total Fuel Quantity: " << engine->GetTotalFuelQuantity() << endl;
	}
	if (tempStr == "print_total_consumed_fuel_quantity;") {
		if (engine->GetNumOfConnectedTank() > 0 && engine->GetStatus())
			engine->AbsorbFuel();

		cout << "Total Consumed Fuel Quantity: " << engine->GetTotalConsumed() << endl;
		engine->GetOutput() << "Total Consumed Fuel Quantity: " << engine->GetTotalConsumed() << endl;

	}
	if (!tempStr.find("print_tank_info")) {
		if (engine->GetNumOfConnectedTank() > 0 && engine->GetStatus())
			engine->AbsorbFuel();

		engine->PrintTankInfo(leftNumberInCommand);
	}
	if (!tempStr.find("fill_tank")) {
		engine->FillTank(leftNumberInCommand, rightNumberInCommand);
		if (engine->GetNumOfConnectedTank() > 0 && engine->GetStatus()) {
			engine->AbsorbFuel();
		}
		
	}
	if (!tempStr.find("open_valve")) {
		engine->OpenValve(leftNumberInCommand);
		if (engine->GetNumOfConnectedTank() > 0 && engine->GetStatus()) {
			engine->AbsorbFuel();
		}
		
	}
	if (!tempStr.find("close_valve")) {
		engine->CloseValve(leftNumberInCommand);
		if (engine->GetNumOfConnectedTank() > 0 && engine->GetStatus()) {
			engine->AbsorbFuel();
		}
	
	}
	if (!tempStr.find("break_fuel_tank")) {
		engine->BreakFuelTank(leftNumberInCommand);
		if (engine->GetNumOfConnectedTank() > 0 && engine->GetStatus()) {
			engine->AbsorbFuel();
		}
		else {
			cout << "The engine is not already running!\n";
			engine->GetOutput() << "The engine is not already running!\n";
		}
	}
	if (!tempStr.find("repair_fuel_tank")) {
		engine->RepairFuelTank(leftNumberInCommand);
		if (engine->GetNumOfConnectedTank() > 0 && engine->GetStatus()) {
			engine->AbsorbFuel();
		}
		else {
			cout << "The engine is not already running!\n";
			engine->GetOutput() << "The engine is not already running!\n";
		}
	}
	if (!tempStr.find("wait")) {
		cout << "Waiting [" << leftNumberInCommand << "]\n";
		if (engine->GetNumOfConnectedTank() > 0 && engine->GetStatus())
			engine->AbsorbFuel(leftNumberInCommand);
	}
	if (tempStr == "stop_simulation;") {
		size_t sz = engine->GetNumOfTank();
		for (int i = 1; i <= sz; i++) {
			Tank* tank = new Tank();
			this->engine->getTank(*tank, i);
			Observer* observe1 = new Observer(*tank);
			observe1->Update("Tank", i);
			this->observe.push_back(observe1);
			Valve* valve = new Valve();
			this->engine->getValve(*valve, i);
			observe1 = new Observer(*valve);
			observe1->Update("Valve", i);
			this->observe.push_back(observe1);
			/*delete tank;
			delete valve;*/
		}
		for (int i = 0; i < observe.size(); i++) {
			observe[i]->Remove();
		}
		exit(0);
	}
	return true;
}

void Input::HowManyQuantity(const string tempStr) { /// This functions reads left and right number in commands and saves.
	int leftIndex = 0, rightIndex = tempStr.find(';');
	string newStr;

	for (int i = 0; i < tempStr.size(); i++) {
		leftIndex++;
		if (tempStr[i] == ' ') {
			break;
		}
	}

	newStr = tempStr.substr(leftIndex, rightIndex - leftIndex);
	if (newStr.size() == 0) {
		return;
	}
	if (newStr.find(' ')) {
		leftNumberInCommand = stod(newStr.substr(0, newStr.find(' ')));
		rightNumberInCommand = stod(newStr.substr(newStr.find(' ') + 1, rightIndex - newStr.find(' ')));
	}
	else {
		leftNumberInCommand = stod(newStr.substr(0, rightIndex));
	}
}
Input::~Input() {
	fileToBeRead.close();
	engine->SetOutput();
}
