#pragma once
#define _SUBJECTINTERFACE_
#include "ObserveInterface.h"
class SubjectInteface {
public:
	virtual ~SubjectInteface() {};
	virtual void Attach(ObserveInterface* observeInterface) = 0;
	virtual void Detach(ObserveInterface* observeInterface) = 0;
	virtual void Notify() = 0;
};